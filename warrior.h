#include "abstract_class.h"

class Warrior : public AbstractClass {
public:
    void action_damage(Warrior *enemy);
    
    uint32_t get_health() const;
    uint32_t get_damage() const;
protected:
    int health, damage;
    uint32_t total_damage, start_life, team, cost;
    
    uint16_t pos_x, pos_y;
};

class InfantryMan : public Warrior {
public:
    InfantryMan() {};
};

class HorseMan : public Warrior {
public:
    HorseMan() {};
};

class WizardMan : public Warrior {
public:
    WizardMan() {};
};

class ArcherMan : public Warrior {
public:
    ArcherMan() {};
};
