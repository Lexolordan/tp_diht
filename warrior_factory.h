#include "abstract_factory.h"
#include "warrior.h"

class InfantryManFactory : public AbstractFactory {
public:
    InfantryMan * create() const {
        return new InfantryMan();
    }
};

class HorseManFactory : public AbstractFactory {
public:
    HorseMan * create() const {
        return new HorseMan();
    }
};

class WizardManFactory : public AbstractFactory {
public:
    WizardMan * create() const {
        return new WizardMan();
    }
};

class ArcherManFactory : public AbstractFactory {
public:
    ArcherMan * create() const {
        return new ArcherMan();
    }
};

class WarriorFactory {
private:
    const InfantryManFactory *imf = new InfantryManFactory();
    const HorseManFactory *hmf = new HorseManFactory();
    const WizardManFactory *wmf = new WizardManFactory();
    const ArcherManFactory *amf = new ArcherManFactory();
public:
    InfantryMan * create_infantry() const {
        return imf->create();
    }
    
    HorseMan * create_horse() const {
        return hmf->create();
    }
    
    ArcherMan * create_archer() const {
        return amf->create();
    }
    
    WizardMan * create_wizard() const {
        return wmf->create();
    }
};

