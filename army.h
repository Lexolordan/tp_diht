class Army {
public:
    void add_infantry();
    void add_horse();
    void add_archer();
    void add_wizard();
private:
    std::vector<InfantryMan *> infantry;
    std::vector<HorseMan *> horse;
    std::vector<ArcherMan *> archer;
    std::vector<WizardMan *> wizard;
    
    uint16_t pos_x, pos_y;
};
