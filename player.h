class Player {
public:
    Player() {}
    
    int get_money() const;
    std::string get_username() const;
private:
    std::vector<Army *> troopers;
    std::string username;
    
    int money;
};
