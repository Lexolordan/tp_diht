class GameDesk {
private:
    static GameDesk *instance;
    static int iterations;
    
    GameDesk() = delete;
    ~GameDesk() = delete;
    
    static void init();
public:
    static GameDesk * get_instance() {
        if (!instance) {
            init();
        }
        return instance;
    }
};
