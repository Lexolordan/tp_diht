class AbstractFactory {
public:
    virtual AbstractClass * create() const = 0;
};
